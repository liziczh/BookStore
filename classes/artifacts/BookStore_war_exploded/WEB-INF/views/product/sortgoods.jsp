<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>商品分类</title>
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/sub-menu.css">
    <style>
        .container {
            background: cornsilk;
            padding:10px 20px 50px;
        }
        a {
            text-decoration: none;
            color: #4d4d4d;
        }
        img {
            width: 100%;
        }
        section {
            width: 100%;
            margin-top: 10px;
            overflow-y: auto;
        }
        section ul {
            border: 1px solid #dbdbdb;
            padding: 0;
            overflow-y: auto;
        }
        /*分类部分*/
        .screen ul li {
            padding: 10px;
            border-bottom: 1px dashed #dbdbdb;
            position: relative;
            line-height: 27px;
            list-style: none;
        }
        .screen ul li:last-child {
            border-bottom: 0;
        }
        .screen li .firstterm{
            position: absolute;
            color: #797979;
            font-size: 16px;
            width: 80px;
            text-align: right;
        }
        .screen li .otherterm {
            display: inline-block;
            padding-left: 100px;
        }
        .ra-brand,.ra-type,.ra-sleeve,.ra-thickness,.ra-style {
            position: relative;
            display: inline-block;
            height: 30px;
            margin: 0 20px;
        }
        input[type=radio]+a {
            display: inline-block;
            padding: 0 15px;
            height: 30px;
            line-height: 30px;
            text-align: center;
        }
        input[type=radio] {
            margin: 0;
            position: absolute;
            width: 100%;
            height: 30px;
            z-index: 10000;
            opacity: 0;
        }
        input[type=radio]:checked+a {
            background-color: #f66060;
            color: #fff;
        }
        .screen1{
            min-height: 40px;
            border: 1px solid #ddd;
            line-height: 40px;
        }
        .screen1 div span{
            display: inline-block;
            padding: 5px 10px;
            margin-left:15px ;
            color: #797979;
        }
        .goodslist ul{
            padding: 10px 0;
        }
        .goodslist ul li{
            list-style: none;
        }
        .goodslist ul li img{
            width: 100%;
        }
        .goodslist ul li p{
            text-align: center;
            margin: 5px 0 ;
            color: #4d4d4d;
        }
    </style>
</head>
<body>
<!--顶部导航-->
<div id="top-part"></div>
<div id="search-part"></div>

<!--主体内容-->
<div class="container">
    <section class="screen">
        <ul>
            <li>
                <span class="firstterm ">类型:</span>
                <div class="otherterm">
                    <div class="ra-type">
                        <input type="radio" name="type" checked value="">
                        <a href="javascript:void(0);">不限</a>
                    </div>
                    <div class="ra-type">
                        <input type="radio" name="type" value="衬衫">
                        <a href="javascript:void(0);">小说</a>
                    </div>
                    <div class="ra-type">
                        <input type="radio" name="type" value="T恤">
                        <a href="javascript:void(0);">文艺</a>
                    </div>
                    <div class="ra-type">
                        <input type="radio" name="type" value="夹克">
                        <a href="javascript:void(0);">历史文化</a>
                    </div>
                    <div class="ra-type">
                        <input type="radio" name="type" value="西装">
                        <a href="javascript:void(0);">经济管理</a>
                    </div>
                    <div class="ra-type">
                        <input type="radio" name="type" value="针织衫">
                        <a href="javascript:void(0);">心理/励志</a>
                    </div>
                </div>
            </li>

        </ul>
    </section>
    <section class="screen1">
        <div>
            <span>您可能还想搜:</span>
            <span>男装衬衫 </span>
            <span>男装短袖 </span>
            <span>男鞋 </span>
            <span>女装 </span>
        </div>
    </section>
    <section class="goodslist" >
        <ul id="goodShow">
            <li class="col-sm-3">
                <a href="#">
                    <img src="../../assets/images/5.jpg" alt="">
                    <p>海澜之家夏季v领纯色修身t恤</p>
                    <p>￥99</p>
                </a>
            </li>
            <li class="col-sm-3">
                <a href="#">
                    <img src="../../assets/images/5.jpg" alt="">
                    <p>海澜之家夏季v领纯色修身t恤</p>
                    <p>￥99</p>
                </a>
            </li> <li class="col-sm-3">
            <a href="#">
                <img src="../../assets/images/5.jpg" alt="">
                <p>海澜之家夏季v领纯色修身t恤</p>
                <p>￥99</p>
            </a>
        </li> <li class="col-sm-3">
            <a href="#">
                <img src="../../assets/images/5.jpg" alt="">
                <p>海澜之家夏季v领纯色修身t恤</p>
                <p>￥99</p>
            </a>
        </li> <li class="col-sm-3">
            <a href="#">
                <img src="../../assets/images/5.jpg" alt="">
                <p>海澜之家夏季v领纯色修身t恤</p>
                <p>￥99</p>
            </a>
        </li>
        </ul>
    </section>
</div>

</body>
<script src="../../assets/js/jquery-1.11.0.min.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $("#top-part").load("top-part.jsp");
    $("#search-part").load("search-part.jsp");
</script>

</html>